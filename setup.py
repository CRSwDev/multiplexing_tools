import setuptools


if __name__ == '__main__':
    setuptools.setup(
        name='bdgutils',
        version='0.4',
        packages=setuptools.find_packages("src"),
        package_dir={'': 'src'},
        package_data={'': ['BDSampleTags.fasta', 'BDSampleTags.gtf',
                           'BDSampleTagsMM.fasta', 'BDSampleTagsMM.gtf']},
        entry_points={
            'console_scripts': [
                'demux_gene_counts = bdgutils.demux_gene_counts:main',
                'create_ref = bdgutils.create_ref:main',
                'mtx_split = bdgutils.mtx_split:main.start'
            ],
        },
        install_requires=['pandas', 'numpy', 'scipy', 'begins', 'tables', 'pysam'],
        setup_requires=[],
        tests_require=['pytest'],
        author='David Rosenfeld',
        author_email='David.Rosenfeld@bd.com',
        description='Utilities for processing data from BD Genomics Sample Multiplex and QC Kits',
        long_description="This package contains tools for processing data generated using the BD Single-cell Multiplexing Kit",
        license = 'CC BY-NC-SA'
)
