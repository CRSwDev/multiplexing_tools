#!/usr/bin/env python
"""
Create AbSeq fasta and gtf files
"""

import sys
import argparse
import os

# structure of the gtf data:
# seqname - name of the chromosome or scaffold; chromosome names can be given with or without the 'chr' prefix. Important note: the seqname must be one used within Ensembl, i.e. a standard chromosome name or an Ensembl identifier such as a scaffold ID, without any additional content such as species or assembly. See the example GFF output below."
# source - name of the program that generated this feature, or the data source (database or project name)"
# feature - feature type name, e.g. Gene, Variation, Similarity"
# start - Start position of the feature, with sequence numbering starting at 1."
# end - End position of the feature, with sequence numbering starting at 1."
# score - A floating point value.
# strand - defined as + (forward) or - (reverse).
# frame - One of '0', '1' or '2'. '0' indicates that the first base of the feature is the first base of a codon, '1' that the second base is the first base of a codon, and so on.."
# attribute - A semicolon-separated list of tag-value pairs, providing additional information about each feature."

def dumpGtf(filename, species):
    out_fasta = filename + '.gtf'
    script_dir = os.path.dirname(__file__)
    if species=='h':
        rel_path = 'BDSampleTags.gtf'
    else:
        rel_path = 'BDSampleTagsMM.gtf'
    with open(os.path.join(script_dir, rel_path)) as fr, open(out_fasta,'w+') as fw:
        for line in fr:
            fw.write(line)


def dumpBDG(filename, species):
    out_fasta = filename + '.fasta'
    script_dir = os.path.dirname(__file__)
    if species=='h':
        rel_path = 'BDSampleTags.fasta'
    else:
        rel_path = 'BDSampleTagsMM.fasta'
    with open(os.path.join(script_dir, rel_path)) as fr, open(out_fasta,'w+') as fw:
        for line in fr:
            fw.write(line)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
        parser = argparse.ArgumentParser()

    # if len(argv) > 0 and argv[0] == 'bdgenome':
    #
        parser.add_argument('--bdgenome', help='generate reference files for sample multiplexing (human)',
                            action='store_true')
        parser.add_argument('--bdgenome_mm', help='generate reference file for sample multiplexing (mus musculus)',
                            action='store_true')
        parser.add_argument('--genome', help='name of output fasta and gtf files', default='BDreference')
        args = parser.parse_args()
        if args.bdgenome:
            if args.genome=='BDreference':
                args.genome='BDSampleTags'
            dumpBDG(args.genome,'h')
            dumpGtf(args.genome,'h')
        elif args.bdgenome_mm:
            if args.genome=='BDreference':
                args.genome='BDSampleTagsMM'
            dumpBDG(args.genome, 'm')
            dumpGtf(args.genome, 'm')


if __name__ == '__main__':
    sys.exit(main(None))
